# README

After updates (see changelog) new parameters could have been added.
Please check the `/app/code/.env` file and compare it to your `/app/data/.env` file and add missing lines.
