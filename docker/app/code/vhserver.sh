#!/bin/bash

cd /run/vhserver-steam/

# BepInEx-specific settings
# NOTE: Do not edit unless you know what you are doing!
####
export DOORSTOP_ENABLE=TRUE
export DOORSTOP_INVOKE_DLL_PATH=./BepInEx/core/BepInEx.Preloader.dll
export DOORSTOP_CORLIB_OVERRIDE_PATH=./unstripped_corlib

export LD_LIBRARY_PATH="./doorstop_libs:$LD_LIBRARY_PATH"
export LD_PRELOAD="libdoorstop_x64.so:$LD_PRELOAD"
####

export LD_LIBRARY_PATH="./linux64:$LD_LIBRARY_PATH"
export SteamAppId=892970

set -o allexport; source /app/data/.env; set +o allexport

echo "=> Starting server PRESS CTRL-C to exit"

printf "=> Server parameters are:
=> ServerName: $SERVERNAME
=> WorldName: $WORLDNAME
=> Password: $PASSWORD
=> Port: $GAME_PORT
=> Public: $PUBLIC
=> SaveInterval: $SAVEINTERVAL
=> Backups: $BACKUPS
=> BackupShort: $BACKUPSHORT
=> BackupLong: $BACKUPLONG
=> Crossplay: $CROSSPLAY
=> ModSupport: $MODSUPPORT
=> EXTRAPARAM: $EXTRAPARAM
"

# Tip: Make a local copy of this script to avoid it being overwritten by steam.
# NOTE: Minimum password length is 5 characters & Password cant be in the server name.
# NOTE: You need to make sure the ports 2456-2458 is being forwarded to your server through your local router & firewall.
if [[ $CROSSPLAY == 1 ]]; then
    echo "=> Starting Valheim Server with crossplay suppoprt - Have fun playing!"
    ./valheim_server.x86_64 -name $SERVERNAME -port $GAME_PORT -world $WORLDNAME -password $PASSWORD -savedir '/app/data/' -saveinterval $SAVEINTERVAL -backups $BACKUPS -backupshort $BACKUPSHORT -backuplong $BACKUPLONG  -crossplay -public $PUBLIC ${EXTRAPARAM:-}
else
    echo "=> Starting Valheim Server with steam support - Have fun playing!"
    ./valheim_server.x86_64 -name $SERVERNAME -port $GAME_PORT -world $WORLDNAME -password $PASSWORD -savedir '/app/data/' -saveinterval $SAVEINTERVAL -backups $BACKUPS -backupshort $BACKUPSHORT -backuplong $BACKUPLONG -public $PUBLIC ${EXTRAPARAM:-}
fi
