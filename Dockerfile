FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# Install deps
RUN apt-get update && \
    apt-get install -y curl wget file tar bzip2 gzip unzip bsdmainutils util-linux ca-certificates binutils bc jq tmux netcat lib32gcc-s1 lib32stdc++6 libsdl2-2.0-0 acl libpulse-dev && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# Make home of cloudron rwx for lgsm
RUN rm /etc/nginx/sites-enabled/* && \
    rm -rf /var/log/nginx && \
    ln -s /run/nginx/log /var/log/nginx && \
    rm -rf /var/lib/nginx && \
    ln -s /run/nginx/lib /var/lib/nginx && \
    mkdir -p /root/.config && \
    ln -s /run/unity3d /root/.config/unity3d && \
    ln -s /root/Steam /app/code/Steam

COPY docker/ /

RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf && \
    sed -e 's,^chmod=.*$,chmod=0760\nchown=cloudron:cloudron,' -i /etc/supervisor/supervisord.conf

CMD [ "/app/code/start.sh" ]
